/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# provider "random" {
#   version = "~> 2.0"
# }

resource "random_string" "company_prefix" {
  length  = 3
  lower   = true
  special = false
  number  = false
  upper   = false
}

resource "random_string" "environment_suffix" {
  length  = 3
  lower   = true
  special = false
  number  = false
  upper   = false
}

resource "random_pet" "main" {
  length    = 1
  separator = "-"
}

module "example" {
  source             = "../../../examples/standard"
  org_id             = var.org_id
  billing_id         = var.billing_id
  project_name       = random_pet.main.id
  folder_id          = var.folder_id
  vpc_host           = var.vpc_host
  company_prefix     = random_string.company_prefix.id
  environment_suffix = random_string.environment_suffix.id
}
