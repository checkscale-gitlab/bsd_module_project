variable "org_id" {
  type        = string
  description = "gcp organisation id"
}

variable "billing_id" {
  type        = string
  description = "billing id"
}

variable "vpc_host" {
  type        = string
  description = "the name of the shared vpc host project"
}

variable "bsd_labels" {
  description = "labels to be applied"
  type        = map(string)
  default = {
    org = "bsd"
  }
}

variable "company_prefix" {
  type        = string
  description = "Will prefix the project name"
}

variable "environment_suffix" {
  type        = string
  description = "if supplied will suffix the project name"
  default     = ""
}

variable "folder_id" {
  type        = string
  description = "The folder the project will be created in - If not supplied will be in the organisation root"
  default     = ""
}

variable "project_name" {
  type        = string
  description = "The base name for the project, final project name will be {company_prefix}-{project_name}-{environment_suffix}"

}
variable "extra_apis" {
  type        = list(string)
  description = "A list of extra apis that should be enabled"
  default     = []

}
