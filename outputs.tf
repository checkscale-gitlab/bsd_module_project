output "project_name" {
  value       = module.bsd-project.project_name
  description = "The final name of the project"
}
output "project_id" {
  value       = module.bsd-project.project_id
  description = "The project ID"
}
