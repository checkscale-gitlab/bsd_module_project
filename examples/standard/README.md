# Standard Example

This example illustrates how to use the `bsd_module_project` module.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| billing\_id | billing id | `string` | n/a | yes |
| bsd\_labels | labels to be applied | `map(string)` | <pre>{<br>  "org": "bsd"<br>}</pre> | no |
| company\_prefix | Will prefix the project name | `string` | n/a | yes |
| environment\_suffix | if supplied will suffix the project name | `string` | `""` | no |
| extra\_apis | A list of extra apis that should be enabled | `list(string)` | `[]` | no |
| folder\_id | The folder the project will be created in | `string` | `""` | no |
| org\_id | gcp organisation id | `string` | n/a | yes |
| project\_name | The base name for the project, final project name will be {company\_prefix}-{project\_name}-{environment\_suffix} | `string` | n/a | yes |
| vpc\_host | the name of the shared vpc host project | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| project\_id | n/a |
| project\_name | The name of the created project |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

To provision this example, run the following from within this directory:
- `terraform init` to get the plugins
- `terraform plan` to see the infrastructure plan
- `terraform apply` to apply the infrastructure build
- `terraform destroy` to destroy the built infrastructure
