/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "org_id" {
  type        = string
  description = "gcp organisation id"
}

variable "billing_id" {
  type        = string
  description = "billing id"
}

variable "vpc_host" {
  type        = string
  description = "the name of the shared vpc host project"
}

variable "bsd_labels" {
  description = "labels to be applied"
  type        = map(string)
  default = {
    org = "bsd"
  }
}

variable "company_prefix" {
  type        = string
  description = "Will prefix the project name"
}

variable "environment_suffix" {
  type        = string
  description = "if supplied will suffix the project name"
  default     = ""
}

variable "folder_id" {
  type        = string
  description = "The folder the project will be created in"
  default     = ""
}

variable "project_name" {
  type        = string
  description = "The base name for the project, final project name will be {company_prefix}-{project_name}-{environment_suffix}"

}
variable "extra_apis" {
  type        = list(string)
  description = "A list of extra apis that should be enabled"
  default     = []

}


