# bsd-project

This module was generated from [terraform-google-module-template](https://github.com/terraform-google-modules/terraform-google-module-template/), which by default generates a module that simply creates a GCS bucket. As the module develops, this README should be updated.

The resources/services/activations/deletions that this module will create/trigger are:

- A google cloud project with good stuff set

## Usage

Basic usage of this module is as follows:

```hcl
module "yoour_project" {
  source             = "https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_project/-/tags/v1.0.0"
  org_id             = var.org_id
  billing_id         = var.billing_id
  project_name       = var.project_name
  folder_id          = var.folder_id
  company_prefix     = var.company_prefix
  environment_suffix = var.environment_suffix
  vpc_host           = var.vpc_host
  extra_apis         = var.extra_apis
}
```

Functional examples are included in the
[examples](./examples/) directory.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| billing\_id | billing id | `string` | n/a | yes |
| bsd\_labels | labels to be applied | `map(string)` | <pre>{<br>  "org": "bsd"<br>}</pre> | no |
| company\_prefix | Will prefix the project name | `string` | n/a | yes |
| environment\_suffix | if supplied will suffix the project name | `string` | `""` | no |
| extra\_apis | A list of extra apis that should be enabled | `list(string)` | `[]` | no |
| folder\_id | The folder the project will be created in - If not supplied will be in the organisation root | `string` | `""` | no |
| org\_id | gcp organisation id | `string` | n/a | yes |
| project\_name | The base name for the project, final project name will be {company\_prefix}-{project\_name}-{environment\_suffix} | `string` | n/a | yes |
| vpc\_host | the name of the shared vpc host project | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| project\_id | The project ID |
| project\_name | The final name of the project |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Requirements

These sections describe requirements for using this module.

### Software

The following dependencies must be available:

- [Terraform][terraform] v0.12
- [Terraform Provider for GCP][terraform-provider-gcp] plugin v2.0

### Service Account

A service account with the following roles must be used to provision
the resources of this module:

- Storage Admin: `roles/storage.admin`

The [Project Factory module][project-factory-module] and the
[IAM module][iam-module] may be used in combination to provision a
service account with the necessary roles applied.

### APIs

A project with the following APIs enabled must be used to host the
resources of this module:

- Google Cloud Storage JSON API: `storage-api.googleapis.com`

The [Project Factory module][project-factory-module] can be used to
provision a project with the necessary APIs enabled.

## Contributing

Refer to the [contribution guidelines](./CONTRIBUTING.md) for
information on contributing to this module.

[iam-module]: https://registry.terraform.io/modules/terraform-google-modules/iam/google
[project-factory-module]: https://registry.terraform.io/modules/terraform-google-modules/project-factory/google
[terraform-provider-gcp]: https://www.terraform.io/docs/providers/google/index.html
[terraform]: https://www.terraform.io/downloads.html
